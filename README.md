# client-manager-spa
Client managing SPA using NodeJS, SQLite, ReactJS

* Clone:
    * `git clone https://tuantr10@bitbucket.org/tuantr10/employee-manager.git`

* Install necessary packages:
    * `npm install -g node-pre-gyp`
    * `npm install -g node-gyp`
    * `npm install`
    * `bower install`

* Fire app:
    * `npm run build`
    * `npm start`
    
* Check the app at http://localhost:8081/